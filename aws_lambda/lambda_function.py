import boto3
import os
from botocore.exceptions import ClientError

server_error = {
    "statusCode": 500,
    "count": 0
}

res = {
    "statusCode": 200,
    "count": 0
}


def lambda_handler(event, context):
    if event == None:
        return server_error

    tableName = os.environ["tablename"]

    dynamodb = boto3.resource("dynamodb", region_name=os.environ["regionname"])
    table = dynamodb.Table(tableName)

    try:
        response = table.get_item(
            Key={
                "id": 1
            }
        )
    except ClientError as e:
        return server_error

    else:
        try:
            count = response["Item"]["count"]
        except KeyError:
            return server_error

        if event["only_count"] == 1:
            res["count"] = count
            return res

        elif event["only_count"] == 0:
            count += 1
            try:
                response = table.put_item(
                    Item={
                        "id": 1,
                        "count": count
                    }
                )

                res["count"] = count
                return res
            except ClientError as e:
                return server_error

        else:
            return server_error
