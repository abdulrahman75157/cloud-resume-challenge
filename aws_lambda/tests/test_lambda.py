import os
import sys
import boto3
import unittest
import xmlrunner
from moto import mock_dynamodb

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from lambda_function import lambda_handler


@mock_dynamodb
class TestLambdaHandler(unittest.TestCase):
    def setUp(self):
        self.tableName = os.environ["tablename"]
        self.params = {
            "TableName": self.tableName,
            "AttributeDefinitions": [
                {"AttributeName": "id", "AttributeType": "N"},
            ],
            "KeySchema": [
                {"AttributeName": "id", "KeyType": "HASH"},
            ],
            "ProvisionedThroughput": {
                "ReadCapacityUnits": 1,
                "WriteCapacityUnits": 1
            }
        }

        self.dynamodb = boto3.resource(
            "dynamodb", region_name=os.environ["regionname"])
        self.table = self.dynamodb.create_table(**self.params)

    def test_lambda_handler_successful_put_item(self):
        event = {
            "only_count": 0
        }

        self.table.put_item(Item={"id": 1, "count": 5})

        result = lambda_handler(event, None)

        self.assertEqual(result["statusCode"], 200)
        self.assertEqual(int(result["count"]), 6)

    def test_lambda_handler_successful_get_item(self):
        event = {
            "only_count": 1
        }

        self.table.put_item(Item={"id": 1, "count": 5})

        result = lambda_handler(event, None)

        self.assertEqual(result["statusCode"], 200)
        self.assertEqual(int(result["count"]), 5)

    def test_server_error(self):
        event = {
            "only_count": 1
        }

        result = lambda_handler(event, None)

        self.assertEqual(result["statusCode"], 500)
        self.assertEqual(int(result["count"]), 0)

    def test_no_event(self):
        result = lambda_handler(None, None)

        self.assertEqual(result["statusCode"], 500)
        self.assertEqual(int(result["count"]), 0)

    def test_only_count_value(self):
        event = {
            "only_count": "asfd"
        }

        result = lambda_handler(event, None)

        self.assertEqual(result["statusCode"], 500)
        self.assertEqual(int(result["count"]), 0)


if __name__ == "__main__":
    with open("./aws_lambda/tests/reports/report.xml", "wb") as output:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=output))
