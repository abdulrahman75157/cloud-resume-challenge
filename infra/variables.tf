variable "region" {
  type    = string
  default = "ap-south-1"
}

variable "table_name" {
  type    = string
  default = "crc_table"
}

variable "api_gateway_name" {
  type    = string
  default = "crc_gateway"
}

variable "api_path" {
  type    = string
  default = "crc_api"
}

variable "allow_origin_name" {
  type    = string
  default = "'https://crc.sarcrc.live'"
}

variable "function_name" {
  type    = string
  default = "crc_lambda"
}

variable "stage_name" {
  type    = string
  default = "v0"
}
