terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "http" {
  }
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

provider "aws" {
  region = var.region
}

# START DYNAMODB
resource "aws_dynamodb_table" "testingtf" {
  name                        = var.table_name
  billing_mode                = "PROVISIONED"
  read_capacity               = 1
  write_capacity              = 1
  hash_key                    = "id"
  deletion_protection_enabled = false
  attribute {
    name = "id"
    type = "N"
  }
}
# END DYNAMODB

# START API GATEWAY
resource "aws_api_gateway_rest_api" "cors_api" {
  name = var.api_gateway_name
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "cors_resource" {
  path_part   = var.api_path
  parent_id   = aws_api_gateway_rest_api.cors_api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.cors_api.id
}
# END API GATEWAY

# START CORS OPTIONS
resource "aws_api_gateway_method" "options_method" {
  rest_api_id   = aws_api_gateway_rest_api.cors_api.id
  resource_id   = aws_api_gateway_resource.cors_resource.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "options_integration" {
  rest_api_id          = aws_api_gateway_rest_api.cors_api.id
  resource_id          = aws_api_gateway_resource.cors_resource.id
  http_method          = aws_api_gateway_method.options_method.http_method
  type                 = "MOCK"
  passthrough_behavior = "WHEN_NO_TEMPLATES"
  depends_on           = [aws_api_gateway_method.options_method]
}

resource "aws_api_gateway_method_response" "options_200" {
  rest_api_id = aws_api_gateway_rest_api.cors_api.id
  resource_id = aws_api_gateway_resource.cors_resource.id
  http_method = aws_api_gateway_method.options_method.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
  depends_on = [aws_api_gateway_method.options_method]
}

resource "aws_api_gateway_integration_response" "options_integration_response" {
  rest_api_id = aws_api_gateway_rest_api.cors_api.id
  resource_id = aws_api_gateway_resource.cors_resource.id
  http_method = aws_api_gateway_method.options_method.http_method
  status_code = aws_api_gateway_method_response.options_200.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'OPTIONS,POST'",
    "method.response.header.Access-Control-Allow-Origin"  = var.allow_origin_name
  }
  depends_on = [aws_api_gateway_method_response.options_200]
}
# END CORS OPTIONS

# START LAMBDA FUNCTION
data "archive_file" "lambda_package" {
  type        = "zip"
  source_dir  = "./../aws_lambda"
  excludes    = setsubtract(fileset("./../aws_lambda/", "**"), ["requirements.txt", "lambda_function.py"])
  output_path = "package.zip"
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "lambda_basic" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = aws_iam_role.iam_for_lambda.name
}

resource "aws_iam_policy" "dynamodb" {
  name = "put_get_access_dynamodb_only_crc_table"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:GetItem",
          "dynamodb:PutItem",
        ]
        Effect   = "Allow"
        Resource = ["arn:aws:dynamodb:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:table/${var.table_name}"]
      },
    ]
  })
}

data "aws_iam_policy_document" "put_get_db" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["dynamodb.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy_attachment" "put_get_db_attachment" {
  policy_arn = aws_iam_policy.dynamodb.arn
  role       = aws_iam_role.iam_for_lambda.name
}

resource "aws_lambda_function" "py_lambda" {
  filename         = "package.zip"
  function_name    = var.function_name
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = "lambda_function.lambda_handler"
  runtime          = "python3.10"
  source_code_hash = data.archive_file.lambda_package.output_base64sha256
  environment {
    variables = {
      tablename  = var.table_name
      regionname = var.region
    }
  }
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.py_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.cors_api.execution_arn}/*/*/*"
}
# END LAMBDA FUNCTION

# START POST
resource "aws_api_gateway_method" "post_method" {
  rest_api_id   = aws_api_gateway_rest_api.cors_api.id
  resource_id   = aws_api_gateway_resource.cors_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "post_integration" {
  rest_api_id             = aws_api_gateway_rest_api.cors_api.id
  resource_id             = aws_api_gateway_resource.cors_resource.id
  http_method             = aws_api_gateway_method.post_method.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  passthrough_behavior    = "WHEN_NO_TEMPLATES"
  uri                     = aws_lambda_function.py_lambda.invoke_arn
  depends_on              = [aws_lambda_function.py_lambda]
}

resource "aws_api_gateway_method_response" "post_200" {
  rest_api_id = aws_api_gateway_rest_api.cors_api.id
  resource_id = aws_api_gateway_resource.cors_resource.id
  http_method = aws_api_gateway_method.post_method.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.post_method]
}

resource "aws_api_gateway_integration_response" "post_integration_response" {
  rest_api_id = aws_api_gateway_rest_api.cors_api.id
  resource_id = aws_api_gateway_resource.cors_resource.id
  http_method = aws_api_gateway_method.post_method.http_method
  status_code = aws_api_gateway_method_response.post_200.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = var.allow_origin_name
  }
  depends_on = [
    aws_api_gateway_method_response.post_200,
    aws_api_gateway_integration.post_integration
  ]
}
# END POST

# START DEPLOYMENT
resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [
    aws_api_gateway_integration.options_integration,
    aws_api_gateway_integration.post_integration,
  ]
  rest_api_id = aws_api_gateway_rest_api.cors_api.id
  stage_name  = var.stage_name
}
# END DEPLOYMENT

output "api_url" {
  value       = format("%s/%s", aws_api_gateway_deployment.deployment.invoke_url, var.api_path)
  description = "Public API URL"
}

output "function_name" {
  value = var.function_name
  description = "Lambda function name"
}
